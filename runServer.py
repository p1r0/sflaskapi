#!/usr/bin/env python3
from random import uniform
from flask import Flask, jsonify, request 
from flask_cors import CORS, cross_origin
from sys import exit
import ssl

app = Flask(__name__)
CORS(app, resources={r"/jsonrpc/*": {"origins": "*"}})

@app.route('/', methods=['GET'])
def test():
    return jsonify({'message':'It Works!'})

@app.route('/jsonrpc', methods=['GET'])
def returnALL():
    datos={
            "voltaje" : uniform(0.0, 6.3),
            "angulo" : uniform(-90.0,90.0)
            }
    return jsonify({'Datos': datos})

#@app.route('/lang/<string:name>', methods=['GET'])
#def returnOne(name):
#    lang = [lang for lang in languages if lang['name'] == name]
#    return jsonify({'language': lang})
#
@app.route('/jsonrpc', methods=['POST'])
@cross_origin(origin='localhost',headers=['Content- Type','Authorization'])
def addOne():
    '''Example POST method'''
    #initial data list
    datos = []
    #get voltaje and angulo from post and set into a dictionary
    voltaje = {'voltaje':request.json['voltaje']}
    datos.append(voltaje)
    angulo = {'angulo':request.json['angulo']}
    datos.append(angulo)
    #return the jesonify version of this
    return jsonify({'Datos' : datos})

if __name__ == '__main__':
    debug = True
    sslOn = input("enable SSL?(YES/NO):")
    if sslOn == "YES":
        ctx = ssl.SSLContext(ssl.PROTOCOL_SSLv23) 
        ctx.load_cert_chain('ssl.cert', 'ssl.key')
        app.run(host='0.0.0.0', port=8080, ssl_context = ctx, debug=debug)
    elif sslOn == "NO":
        app.run(host='0.0.0.0', port=8080, debug=debug)
    else:
        print("Unknown option, bye ;)")
        exit(0)
