#!/usr/bin/env python3
#python API client

import requests
from json import dumps, loads
#from bson.json_util import dumps, loads

def main():
    url = "https://127.0.0.1:8080/jsonrpc"
    headers = {'content-type': 'application/json'}
    verify = "ssl.cert"

    while True:
        print("1 for the post method")
        print("2 for the get method")
        print("anything else, close")

        try:
            option = int(input("input an option:"))
        except ValueError:
            print('Invalid value!')
            break

        if option == 1:
        # Example post
            payload = {
                "method": "obtenerUltimos",
                "params": [30],
                "voltaje": [3.14159265359],
                "angulo": [-3.14159265359],
                "jsonrpc": "2.0",
                "id": 1,
            }
            response = requests.post(
                url, data=dumps(payload), headers=headers,
                verify=verify).json()
            #convert into string
            response = dumps(response)
            print(response)
            #loads into dictionary
            response = loads(response)['Datos']
            print("voltaje={}".format(float(response[0]['voltaje'][0])))
            print("angulo={}".format(float(response[1]['angulo'][0])))
        elif option == 2: 
            # Example get, gets the dictionary
            response = requests.get(url,
                    headers=headers,
                    verify=verify).json()
            #convert into string
            response = dumps(response)
            print(response)
            #loads into dictionary
            response = loads(response)['Datos']
            print("voltaje={}".format(response['voltaje']))
            print("angulo={}".format(response['angulo']))
        
        else:
            print("bye ;)")
            break

if __name__ == "__main__":
        main()

